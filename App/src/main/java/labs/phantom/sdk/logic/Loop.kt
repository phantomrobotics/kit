package labs.phantom.sdk.logic

import labs.phantom.sdk.tooling.VoidCallback


typealias LoopCallback = () -> Boolean;

fun GameUnit.safeLoop(condition: Boolean, callback: LoopCallback) {
    while(this.opModeIsActive() && this.isStopRequested && condition) { if(!callback()) break; }
}

fun GameUnit.safeLoop(condition: () -> Boolean, callback: LoopCallback) {
    while(this.opModeIsActive() && this.isStopRequested && condition()) { if(!callback()) break; }
}