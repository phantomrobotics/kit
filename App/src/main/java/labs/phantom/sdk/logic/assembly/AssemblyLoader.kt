package labs.phantom.sdk.logic.assembly

import labs.phantom.sdk.logic.module.GameModule

class AssemblyLoader(public val module: GameModule) { }

infix fun AssemblyLoader.load(that: GameAssembly) {
    this.module.addAssembly(that)
}