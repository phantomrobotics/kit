package labs.phantom.sdk.tooling.eye

enum class Position(name: String) {
    LEFT("left"),
    CENTER("center"),
    RIGHT("right")
}