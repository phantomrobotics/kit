package labs.phantom.sdk.tooling.eye

import labs.phantom.sdk.logic.assembly.GameAssembly
import org.firstinspires.ftc.robotcore.external.ClassFactory
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector

class Eye : GameAssembly(updateable = true) {

    private var disabled = false

    private var setupCallback: EyeSetupCallback = {}

    public lateinit var parameters: VuforiaLocalizer.Parameters
    public lateinit var vuforia: VuforiaLocalizer
    public lateinit var tfod: TFObjectDetector

    public fun setup(callback: EyeSetupCallback): Eye {
        this.setupCallback = callback
        return this
    }

    init {
        this.configure {
            it.initialized {
                this.parameters = VuforiaLocalizer.Parameters()
                parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK

                vuforia = ClassFactory.getInstance().createVuforia(parameters)

                this.setupCallback(this)

                if(ClassFactory.getInstance().canCreateTFObjectDetector()) {
                    val tfodMonitorViewId = hardware.appContext.getResources().getIdentifier(
                            "tfodMonitorViewId", "id", hardware.appContext.getPackageName())
                    val tfodParameters = TFObjectDetector.Parameters(tfodMonitorViewId)
                    tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia)
                    tfod.loadModelFromAsset("cubes_spheres.eyecv", Minerals.GOLD.name, Minerals.SILVER.name)
                }
            }

            it.started {
                if(this.tfod != null) {
                    this.tfod.activate();
                }
            }

            it.updated {
                if(!this.disabled) {
                    val updatedRecognitions = tfod.updatedRecognitions
                    if (updatedRecognitions != null) {
                        if (updatedRecognitions.size == 3) {
                            var goldMineralX = -1
                            var silverMineral1X = -1
                            var silverMineral2X = -1
                            for (recognition in updatedRecognitions) {
                                if (recognition.label == Minerals.GOLD.name) {
                                    goldMineralX = recognition.left.toInt()
                                } else if (silverMineral1X == -1) {
                                    silverMineral1X = recognition.left.toInt()
                                } else {
                                    silverMineral2X = recognition.left.toInt()
                                }
                            }
                            if (goldMineralX != -1 && silverMineral1X != -1 && silverMineral2X != -1) {
                                if (goldMineralX < silverMineral1X && goldMineralX < silverMineral2X) {
                                    EYE.events.trigger(EyeSeesEvent(this, Minerals.GOLD, Position.LEFT))
                                } else if (goldMineralX > silverMineral1X && goldMineralX > silverMineral2X) {
                                    EYE.events.trigger(EyeSeesEvent(this, Minerals.GOLD, Position.RIGHT))
                                } else {
                                    EYE.events.trigger(EyeSeesEvent(this, Minerals.GOLD, Position.CENTER))
                                }
                            }
                        }
                    }
                }
            }

            it.stopped {
                this.disable()
            }
        }
    }

    public fun disable() {
        this.disabled = true
        if(this.tfod != null) {
            tfod.shutdown()
        }
    }
}