package labs.phantom.sdk.events

abstract class GameEventEmitter<T: GameEvent> {
    private val callbackList: MutableList<GameEventCallback<T>> = mutableListOf()

    public fun register(callback: GameEventCallback<T>) {
        this.callbackList.add(callback)
    }

    public fun trigger(e: T) {
        for(callback in this.callbackList) {
            callback(e)
        }
    }

    public fun clear() {
        this.callbackList.clear()
    }
}