package labs.phantom.sdk.hardware

import com.qualcomm.robotcore.hardware.HardwareDevice
import com.qualcomm.robotcore.hardware.HardwareMap

@Suppress("UNCHECKED_CAST")
infix fun <T: HardwareDevice> HardwareMap.export(that: String): T {
    return this.get(that) as T
}