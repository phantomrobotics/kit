package labs.phantom.sdk.hardware.assemblies

import com.qualcomm.hardware.bosch.BNO055IMU
import labs.phantom.sdk.logic.assembly.GameAssembly
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference
import org.firstinspires.ftc.robotcore.external.navigation.Orientation

typealias IMUSetupCallback = (u: IMUAssembly) -> Unit;

class IMUAssembly(public val name: String = "imu") : GameAssembly(updateable = true) {
    public lateinit var parameters: BNO055IMU.Parameters
    public lateinit var imu: BNO055IMU

    public var axesReference: AxesReference = AxesReference.INTRINSIC
    public var axesOrder: AxesOrder = AxesOrder.ZYX
    public var angleUnit: AngleUnit = AngleUnit.DEGREES

    private var setupCallback: IMUSetupCallback = {}

    public fun setup(callback: IMUSetupCallback): IMUAssembly {
        this.setupCallback = callback
        return this
    }

    init {
        this.configure {
            it.initialized {
                this.parameters = BNO055IMU.Parameters()

                this.parameters.mode                = BNO055IMU.SensorMode.IMU;
                this.parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
                this.parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
                this.parameters.loggingEnabled      = false;

                this.setupCallback(this)

                this.imu = hardware.get(BNO055IMU::class.java, this.name)
                this.imu.initialize(this.parameters)
            }
        }
    }

    public fun get(): Orientation {
        return this.imu.getAngularOrientation(this.axesReference, this.axesOrder, this.angleUnit);
    }

    public fun getAngle(): Double {
        return this.get().firstAngle.toDouble()
    }

    public fun status(): BNO055IMU.CalibrationStatus {
        return this.imu.calibrationStatus;
    }

    public fun waitForCalibration() {
        while (!this.imu.isGyroCalibrated) {
            unit.sleep(50)
            unit.idle()
        }
    }
}