package labs.phantom.sdk.hardware.gamepad

enum class Gamepads {
    FIRST,
    SECOND
}