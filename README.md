# Phantom SDK for Android Studio/Intellij IDEA

This is the SDK. You can find more info and tutorials [here](https://iamthevex.github.io/phantom/).
If you want to clone the repo you might want to use --depth=1 to download only the most recent version (or you might just download the zip if you're not very experienced with git).

You can also clone this repo directly from Android Studio/IDEA.
